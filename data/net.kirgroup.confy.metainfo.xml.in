<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
	<id>net.kirgroup.confy</id>
	<name>Confy</name>
	<developer id="net.kirgroup">
		<name>Fabio Comuni</name>
	</developer>
	<update_contact>~fabrixxm/confy-dev@lists.sr.ht</update_contact>
	<summary>Conference schedules viewer</summary>
	<description>
		<p>
			Confy lets you browse conference schedules.
		</p>
		<p>
			Browse talks by day, track or room.
			Select the talks you are interested in and receive notification when they are about to start.
			View when two or more talks you are interested in overlap.
		</p>
	</description>
	<launchable type="desktop-id">net.kirgroup.confy.desktop</launchable>
	<metadata_license>CC-BY-4.0</metadata_license>
	<project_license>GPL-3.0-or-later</project_license>
	<url type="homepage">https://confy.kirgroup.net</url>
	<url type="bugtracker">https://todo.sr.ht/~fabrixxm/confy</url>
	<screenshots>
		<screenshot type="default">
			<image>https://confy.kirgroup.net/appdata/confy1.png</image>
			<caption>Confy start view with recently opened conferences</caption>
		</screenshot>
		<screenshot>
			<image>https://confy.kirgroup.net/appdata/confy2.png</image>
			<caption>Starred talks</caption>
		</screenshot>
		<screenshot>
			<image>https://confy.kirgroup.net/appdata/confy3.png</image>
			<caption>Talk details in mobile size</caption>
		</screenshot>
	</screenshots>
	<content_rating type="oars-1.1"/>
	<custom>
		<value key="Purism::form_factor">mobile</value>
	</custom>
	<supports>
		<control>pointing</control>
		<control>keyboard</control>
		<control>touch</control>
	</supports>
	<requires>
		<display_length compare="ge">360</display_length>
	</requires>
	<releases>
		<release version="0.8.0" date="2025-01-03">
			<description>
				<p>Show recent opened conferences in star page</p>
				<p>Show speakers details and avatars, if available</p>
				<p>User-selectable talks groupping in lists</p>
				<p>Updated schedules parser</p>
				<p>Updated UI</p>
			</description>
		</release>
		<release version="0.7.1" date="2024-01-28">
			<description>
				<p>German translation, thanks to Sebastian Crane</p>
				<p>Fix day format in Swedish translation</p>
				<p>Update README</p>
				<p>Fix Meson build scripts</p>
				<p>Flatpak: bump to GNOME Runtime 45</p>
			</description>
		</release>
		<release version="0.7.0" date="2023-07-25">
			<description>
				<p>Updated to Gtk4/libadwaita</p>
				<p>Reviseted user interaction, dialogs and shortcuts</p>
				<p>Copy talk detail into clipboard as plaintext from menu or shortcut</p>
				<p>Fix events that are after UTC midnight</p>
			</description>
		</release>
		<release version="0.6.5" date="2023-06-01">
			<description>
				<p>French tranlsation, thanks to Irénée Thirion</p>
				<p>Swedish translation, thanks to Luna Jernberg</p>
				<p>Fix Pentabarf date parsing</p>
				<p>Flatpak: bump Gnome Runtime to verion 44</p>
			</description>
		</release>
		<release version="0.6.4" date="2022-08-17">
		<description>
				<p>Convert markdown links to html hyperlinks</p>
				<p>Don't track visited links in labels</p>
				<p>Make labels selectable</p>
				<p>Margin in loading widget: no more loadbar touching screen edges in mobile</p>
				<p>Event text justified to left</p>
				<p>Use symbolic icon in search placeholder</p>
			</description>
		</release>
		<release version="0.6.3" date="2021-12-29">
			<description>
				<p>Update metainfo data with input support and display requirement.</p>
			</description>
		</release>
		<release version="0.6.2" date="2021-11-10">
			<description>
				<p>Update appearance to better conform to Gnome</p>
			</description>
		</release>
		<release version="0.6.1" date="2021-07-02">
			<description>
				<p>Fix event progressbar for events with zero duration</p>
				<p>Display events talks datetimes in current timezone</p>
				<p>Declare support for mobile devices in metadata</p>
			</description>
		</release>
		<release version="0.6.0" date="2021-06-12">
			<description>
				<p>Search talks in schedule and starred view</p>
				<p>Better page navigation in starred view</p>
				<p>Varius enhancement to UI to better fit GNOME guidelines</p>
			</description>
		</release>
		<release version="0.5.5" date="2021-04-06">
			<description>
				<p>Open urls with default application per file type, if possible.</p>
			</description>
		</release>
		<release version="0.5.4" date="2021-02-18">
			<description>
				<p>Fix "next up" query and notification.</p>
				<p>Fix text rendering issues with unsupported markup tags.</p>
			</description>
		</release>
		<release version="0.5.3" date="2021-01-28">
			<description>
				<p>Add event progress indicator.</p>
				<p>Fix overlapping events definition: exclude events ending/starting on same exact time.</p>
			</description>
		</release>
		<release version="0.5.2" date="2021-01-17"/>
		<release version="0.5.1" date="2020-12-30"/>
		<release version="0.5.0" date="2020-12-27"/>
		<release version="0.4.1" date="2020-12-14"/>
		<release version="0.3.0" date="2020-03-19"/>
	</releases>
</component>


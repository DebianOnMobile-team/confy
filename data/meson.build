pkgdatadir = join_paths(get_option('prefix'), get_option('datadir'), meson.project_name())

iconsdir = join_paths(get_option('datadir'), 'icons', 'hicolor')
foreach size: ['scalable', 'symbolic']
  install_subdir('icons/' + size, install_dir: iconsdir)
endforeach


gnome = import('gnome')
gnome.compile_resources('confy.data',
  'data.gresource.xml',
  gresource_bundle: true,
  install: true,
  install_dir: pkgdatadir,
)


desktop_file = i18n.merge_file(
  input: 'net.kirgroup.confy.desktop.in',
  output: 'net.kirgroup.confy.desktop',
  type: 'desktop',
  po_dir: '../po',
  install: true,
  install_dir: join_paths(get_option('datadir'), 'applications')
)

desktop_utils = find_program('desktop-file-validate', required: false)
if desktop_utils.found()
  test('Validate desktop file', desktop_utils,
    args: [desktop_file]
  )
endif

metainfo_file = i18n.merge_file(
    input:  'net.kirgroup.confy.metainfo.xml.in',
    output: 'net.kirgroup.confy.metainfo.xml',
    type: 'xml',
    po_dir: join_paths (meson.project_source_root(), 'po'),
    install: true,
    install_dir: join_paths (get_option ('datadir'), 'metainfo')
)

appstream_cli = find_program('appstream-util', required: false)
if appstream_cli.found()
  test('Validate appstream file', appstream_cli,
    args: ['validate', '--nonet', metainfo_file]
  )
endif


install_data('net.kirgroup.confy.gschema.xml',
  install_dir: join_paths(get_option('datadir'), 'glib-2.0/schemas')
)

compile_schemas = find_program('glib-compile-schemas', required: false)
if compile_schemas.found()
  test('Validate schema file', compile_schemas,
    args: ['--strict', '--dry-run', meson.current_source_dir()]
  )
endif

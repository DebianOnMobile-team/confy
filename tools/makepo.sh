#!/bin/bash
set -euo pipefail
cd "$(dirname "$0")/.."
[ ! -d bld ] && meson setup bld
ninja -C bld/ confy-pot
ninja -C bld/ confy-update-po
rm -r bld

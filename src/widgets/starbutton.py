# widgets/starbutton.py
#
# Copyright 2020-2023 Fabio Comuni, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gi.repository import GObject
from gi.repository import Gtk


@Gtk.Template(resource_path="/net/kirgroup/confy/widgets/starbutton.ui")
class StarButton(Gtk.Button):
    __gtype_name__ = "ConfyStarButton"

    _is_starred = False

    @GObject.Property(type=bool, default=True)
    def is_starred(self):
        return self._is_starred

    @is_starred.setter
    def is_starred_setter(self, v):
        self._is_starred = v
        self.notify("icon-name")

    @GObject.Property(type=str, default="non-starred-symbolic")
    def icon_name(self):
        return "starred-symbolic" if self._is_starred else "non-starred-symbolic"

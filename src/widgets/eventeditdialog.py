# widgets/eventdialog.py
#
# Copyright 2020-2023 Fabio Comuni, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gi.repository import Adw
from gi.repository import GObject
from gi.repository import Gtk


@Gtk.Template(resource_path="/net/kirgroup/confy/widgets/eventeditdialog.ui")
class EventEditDialog(Adw.Dialog):
    __gtype_name__ = "ConfyEventEditDialog"

    CANCEL = Gtk.ResponseType.CANCEL
    OK = Gtk.ResponseType.OK
    DELETE = 1

    _can_save = False
    _can_delete = False
    _can_cancel = False

    @GObject.Property(type=bool, default=False)
    def can_save(self):
        return self._can_save

    @can_save.setter
    def can_save_setter(self, value):
        self._can_save = value

    @GObject.Property(type=bool, default=False)
    def can_delete(self):
        return self._can_delete

    @can_delete.setter
    def can_delete_setter(self, value):
        self._can_delete = value

    @GObject.Property(type=bool, default=False)
    def can_cancel(self):
        return self._can_cancel

    @can_cancel.setter
    def can_cancel_setter(self, value):
        self._can_cancel = value

    @GObject.Signal
    def save(self):
        pass

    @GObject.Signal
    def cancel(self):
        pass

    @GObject.Signal
    def delete(self):
        pass

    entry_url = Gtk.Template.Child()  # type: ignore
    entry_title = Gtk.Template.Child()  # type: ignore
    entry_start = Gtk.Template.Child()  # type: ignore
    entry_end = Gtk.Template.Child()  # type: ignore
    entry_icon = Gtk.Template.Child()  # type: ignore

    def __init__(self, parent, conf, **kwargs):
        self._err = set()
        self.conf = conf
        super().__init__(**kwargs)

        self.setup_ui()

    def get_conf(self):
        return self.conf

    def _do_ok(self):
        self.conf.url = self.entry_url.get_text()
        self.conf.title = self.entry_title.get_text()
        self.conf.start = self.entry_start.get_date()
        self.conf.end = self.entry_end.get_date()

        if self.conf.metadata is None:
            self.conf.metadata = {}

        if self.entry_icon.get_text() != "":
            self.conf.metadata['icon'] = self.entry_icon.get_text()
        elif 'icon' in self.conf.metadata:
            del self.conf.metadata['icon']

    @Gtk.Template.Callback()
    def save_event(self, entry):
        self._do_ok()
        self.emit("save")

    @Gtk.Template.Callback()
    def cancel_event(self, entry):
        self.emit("cancel")

    @Gtk.Template.Callback()
    def delete_event(self, entry):
        self.emit("delete")

    @Gtk.Template.Callback()
    def validate_entry_required(self, entry):
        if len(entry.get_text()) > 0:
            entry.remove_css_class("error")
            if entry in self._err:
                self._err.remove(entry)
        else:
            entry.add_css_class("error")
            self._err.add(entry)
        self._update_ok()

    @Gtk.Template.Callback()
    def validate_date_order(self, entry):
        if self.entry_start.get_date() is None or self.entry_end.get_date() is None:
            entry.add_css_class("error")
            self._err.add(entry)
        elif self.entry_start.get_date() > self.entry_end.get_date():
            entry.add_css_class("error")
            self._err.add(entry)
        else:
            for e in (self.entry_start, self.entry_end):
                e.remove_css_class("error")
                if e in self._err:
                    self._err.remove(e)
        self._update_ok()

    def _update_ok(self):
        self.props.can_save = len(self._err) == 0

    def setup_ui(self):
        self.entry_url.set_text(self.conf.url)
        self.entry_title.set_text(self.conf.title)
        self.entry_start.set_date(self.conf.start)
        self.entry_end.set_date(self.conf.end)

        if self.conf.metadata is not None:
            self.entry_icon.set_text(self.conf.metadata.get('icon', ''))

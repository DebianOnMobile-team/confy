# widgets/inputdialog.py
#
# Copyright 2020-2023 Fabio Comuni, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from gettext import gettext as _

from gi.repository import Gtk
from gi.repository import Adw


class InputDialog(Adw.MessageDialog):
    CANCEL = "cancel"
    OK = "ok"

    def __init__(self, parent, **kwargs):
        super().__init__(transient_for=parent)
        self.props.heading = _("Add Event from URL")
        self.props.body = _("Enter URL to event schedule to add it to the list.\nConfy supports events in Pentabarf (.xml) or iCal (.ics) format.")

        self.add_response(self.CANCEL, _("Cancel"))
        self.add_response(self.OK, _("Add"))
        self.set_response_appearance(self.OK, Adw.ResponseAppearance.SUGGESTED)
        self.set_default_response(self.OK)
        self.set_close_response(self.CANCEL)

        self.props.extra_child = Gtk.Entry(
            activates_default=True,
            editable=True,
        )

    def get_text(self):
        return self.props.extra_child.get_text()

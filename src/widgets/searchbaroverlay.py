# widgets/searchbaroverlay.py
#
# Copyright 2020-2023 Fabio Comuni, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import GObject
from gi.repository import Gtk


@Gtk.Template(resource_path="/net/kirgroup/confy/widgets/searchbar.ui")
class SearchBarOverlay(Gtk.Box):
    """ Ora, mi piacerebbe sapere perchè cazzo Gtk.SearchBox è final,
        che chiaramente significa che non è subclassabile, solo che pare che
        posso farlo (almeno in PyGObject), ma scazza con il template.
        In pratica me lo costruisce, ma poi sparisce il child.
        Quindi sono costretto a wrapparlo in un elemento che posso subclassare.
        Ma dato che Gtk.Widget non ha child, la cosa più veloce è usare un Box.
        Così adesso il Box deve calcolare la dimensione dell'unico child che ha,
        la stracazzo di Gtk.SearchBox.
        E siccome questa doveva essere una subclass di Gtk.SearchBar, mi aspetto che
        si comporti come una Gtk.SearchBar e quindi sono costretto a riportare
        le properties e i metodi che uso nel codice.
        Bella merda.
    """
    __gtype_name__ = "ConfySearchBarOverlay"

    bar = Gtk.Template.Child()  # type: ignore
    entry = Gtk.Template.Child()  # type: ignore
    is_starred = Gtk.Template.Child()  # type: ignore

    @GObject.Signal(name="changed")
    def changed(self):
        ...

    _search_mode_enabled = False
    _show_close_button = False
    _show_advanced = True

    @GObject.Property(type=bool, default=False)
    def search_mode_enabled(self):
        return self._search_mode_enabled

    @search_mode_enabled.setter
    def search_mode_enabled_setter(self, value):
        self._search_mode_enabled = value

    @GObject.Property(type=bool, default=True)
    def show_close_button(self):
        return self._show_close_button

    @show_close_button.setter
    def show_close_button_setter(self, value):
        self._show_close_button = value

    @GObject.Property(type=bool, default=True)
    def show_advanced(self):
        return self._show_advanced

    @show_advanced.setter
    def show_advanced_setter(self, value):
        self._show_advanced = value

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.bind_property('search-mode-enabled', self.bar, 'search-mode-enabled', GObject.BindingFlags.BIDIRECTIONAL)
        self.bind_property('show-close-button', self.bar, 'show-close-button', GObject.BindingFlags.BIDIRECTIONAL)
        self.bar.connect('notify::search-mode-enabled', self._on_search_mode_changed)

    def _on_search_mode_changed(self, *args):
        if self.bar.get_search_mode():
            self.entry.set_text("")
            self.is_starred.set_active(False)
            self.entry.grab_focus()

    def set_search_mode(self, value):
        self.bar.set_search_mode(value)

    def is_searching(self):
        return self.bar.get_search_mode() and (self.entry.get_text() != "" or self.is_starred.get_active())

    @Gtk.Template.Callback()
    def send_changed(self, *_):
        self.changed.emit()

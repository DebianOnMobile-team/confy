# mainwindow.py
#
# Copyright 2020-2024 Fabio Comuni, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gettext import gettext as _

from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import Adw


class ToggleSidebarButton(Gtk.ToggleButton):
    __gtype_name__ = "ConfyToggleSidebarButton"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.connect("realize", self._on)

    def _on(self, *args):
        # find Adw.OverlaySplitView
        parent = self
        while parent.get_parent() is not None:
            parent = parent.get_parent()
            if parent.__class__ == Adw.OverlaySplitView:
                parent.bind_property("collapsed", self, "visible", GObject.BindingFlags.SYNC_CREATE)
                parent.bind_property("show-sidebar", self, "active", GObject.BindingFlags.BIDIRECTIONAL)

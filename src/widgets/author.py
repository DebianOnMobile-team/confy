# widgets/author.py
#
# Copyright 2020-2024 Fabio Comuni, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from gi.repository import Gio
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Adw

from .. import models
from .. import local
from ..utils import clean_markup


class Author(Gtk.Box):
    __gtype_name__ = "ConfyAuthor"

    def __init__(self, person: models.Person):
        super().__init__(spacing=8)
        self.person = person

        name = clean_markup(person.name)

        self.avatar = avatar = Adw.Avatar.new(24, name, True)
        self.append(avatar)

        self.label = label = Gtk.Label(label=name)
        label.props.halign = Gtk.Align.START
        self.append(label)

        print(person.thumbnail)
        if person.thumbnail:
            self._f = local.get_image_async(person.thumbnail, self._set_image)

    def _set_image(self, fetcher, localfile):
        self._f = None
        self.avatar.set_custom_image(
            Gdk.Texture.new_from_file(
                Gio.File.new_for_path(localfile)
            )
        )

pkgdatadir = join_paths(get_option('prefix'), get_option('datadir'), meson.project_name())
gnome = import('gnome')

python = import('python')
python3 = python.find_installation('python3')
pythondir = python3.get_install_dir()
moduledir = join_paths(pythondir, 'confy')


subdir('widgets')
subdir('pages')
subdir('migrations')
subdir('remotes')


## blueprint
blueprints_src	 = [
    'openwindow.blp',
    'mainwindow.blp',
    'shortcuts.blp',
]

blueprints_ui = []
foreach b : blueprints_src
	blueprints_ui += b.replace('.blp', '.ui')
endforeach

blueprints = custom_target('blueprints',
  input: blueprints_src,
  output: blueprints_ui,
  command: [blueprint_compiler, 'batch-compile', '@OUTDIR@', '@CURRENT_SOURCE_DIR@', '@INPUT@'],
)
## /blueprint

gnome.compile_resources('confy',
  'confy.gresource.xml',
  dependencies: [blueprints, widgets_blueprints, pages_blueprints],
  gresource_bundle: true,
  install: true,
  install_dir: pkgdatadir,
  source_dir: 'src/',
)


conf = configuration_data()
conf.set('PYTHON', python3.full_path())
conf.set('VERSION', meson.project_version())
conf.set('localedir', join_paths(get_option('prefix'), get_option('localedir')))
conf.set('pkgdatadir', pkgdatadir)
conf.set('pythondir', pythondir)

configure_file(
  input: 'confy.in',
  output: 'confy',
  configuration: conf,
  install: true,
  install_mode: 'rwxr-xr-x',
  install_dir: get_option('bindir')
)

configure_file(
  input: 'config.py.in',
  output: 'config.py',
  configuration: conf,
  install: true,
  install_dir: moduledir
)

confy_sources = [
  '__init__.py',
  'main.py',
  'openwindow.py',
  'mainwindow.py',
  'models.py',
  'local.py',
  'settings.py',
  'fetcher.py',
  'preferences.py',
  'utils.py',
  'networkconnectionmonitor.py',
]

install_data(confy_sources, install_dir: moduledir)


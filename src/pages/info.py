# info.py
#
# Copyright 2020-2024 Fabio Comuni, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import Optional

from gi.repository import GObject
from gi.repository import Gio
from gi.repository import Gdk
from gi.repository import Gtk
from gi.repository import Adw

from .. import models
from ..config import FMT_DAY  # type:ignore # config is generated at compile time
from ..widgets import WebLinkActionRow, weblinkactionrowactivated
from ..utils import clean_markup


@Gtk.Template(resource_path="/net/kirgroup/confy/pages/info.ui")
class PageInfo(Adw.NavigationPage):
    __gtype_name__ = "ConfyPageInfo"

    conf: Optional[models.Conference] = None
    meta: Optional[models.Meta] = None

    statuspage = Gtk.Template.Child()  # type: ignore
    linkbox = Gtk.Template.Child()  # type: ignore

    @GObject.Property(type=str, default="")
    def description(self):
        desc = self.meta.venue or ""
        if self.meta.venue and self.meta.city:
            desc += "\n"
        desc += self.meta.city or ""
        if desc != "":
            desc += "\n\n"
        desc += self.meta.start.strftime(FMT_DAY)
        if self.meta.start != self.meta.end:
            desc += "\n" + self.meta.end.strftime(FMT_DAY)
        return clean_markup(desc)

    def __init__(self, win, conf, **kwargs):
        super().__init__(**kwargs)
        self.conf = conf
        self.update()
        win.connect("conf-updated", self.update)

    def _set_logo(self, fetcher, logofile):
        self.statuspage.set_paintable(
            Gdk.Texture.new_from_file(
                Gio.File.new_for_path(logofile)
            )
        )

    def update(self, *args):
        self.meta = self.conf.get_meta()
        if self.meta and self.meta.title:
            print("INFO set title to", self.meta.title)
            self.set_title(clean_markup(self.meta.title))

        self.statuspage.set_icon_name('net.kirgroup.confy-symbolic')
        self.conf.get_logo_file(cbk=self._set_logo)

        w = self.linkbox.get_last_child()
        while w:
            self.linkbox.remove(w)
            w = self.linkbox.get_last_child()

        links = self.conf.metadata.get("links", [])
        self.linkbox.set_visible(len(links) > 0)
        for link in links:
            row = WebLinkActionRow(link['title'], link['url'])
            self.linkbox.append(row)

        self.notify("description")

    @Gtk.Template.Callback()
    def on_linkbox_row_activated(self, listbox, row):
        weblinkactionrowactivated(listbox, row)

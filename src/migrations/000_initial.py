def up(cursor):
    cursor.execute("""CREATE TABLE IF NOT EXISTS meta (
        key TEXT(20) PRIMARY KEY,
        value TEXT)""")
    cursor.execute("""CREATE TABLE IF NOT EXISTS persons (
        id TEXT(50) PRIMARY KEY,
        name TEXT)""")
    cursor.execute("""CREATE TABLE IF NOT EXISTS event_person (
        event_id TEXT(50),
        person_id TEXT(50),
        FOREIGN KEY(event_id) REFERENCES events(id),
        FOREIGN KEY(person_id) REFERENCES persons(id))""")
    cursor.execute("""CREATE TABLE IF NOT EXISTS events (
        id TEXT(50) PRIMARY KEY,
        date DATE,
        start TIMESTAMP,
        end TIMESTAMP,
        room TEXT DEFAULT "",
        slug TEXT,
        title TEXT,
        subtitle TEXT,
        track TEXT DEFAULT "",
        type TEXT,
        abstract TEXT,
        description TEXT,
        starred INTEGER(1) DEFAULT 0,
        notified INTEGER(1) DEFAULT 0)""")
    cursor.execute("CREATE INDEX IF NOT EXISTS event_date ON events(date)")
    cursor.execute("CREATE INDEX IF NOT EXISTS event_room ON events(room)")
    cursor.execute("CREATE INDEX IF NOT EXISTS event_track ON events(track)")
    cursor.execute("CREATE INDEX IF NOT EXISTS event_starred ON events(starred)")
    cursor.execute("""CREATE TABLE IF NOT EXISTS links (
        event_id TEXT(50),
        href TEXT,
        name TEXT,
        FOREIGN KEY(event_id) REFERENCES events(id))""")
    cursor.execute("CREATE UNIQUE INDEX IF NOT EXISTS name_event ON links(event_id, name)")


def drop(cursor):
    # Do I need this?
    ...

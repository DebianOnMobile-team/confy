def up(cursor):
    cursor.execute("ALTER TABLE persons ADD COLUMN organization TEXT DEFAULT '';")
    cursor.execute("ALTER TABLE persons ADD COLUMN thumbnail TEXT DEFAULT '';")
    cursor.execute("ALTER TABLE persons ADD COLUMN bio TEXT DEFAULT '';")
    cursor.execute("ALTER TABLE persons ADD COLUMN url TEXT DEFAULT '';")


def down(cursor):
    ...

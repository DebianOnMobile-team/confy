def up(cursor):
    cursor.execute("DROP TABLE IF EXISTS event_person")
    cursor.execute("""CREATE TABLE event_person (
        event_id TEXT(50),
        person_id TEXT(50),
        FOREIGN KEY(event_id) REFERENCES events(id),
        FOREIGN KEY(person_id) REFERENCES persons(id),
        UNIQUE(event_id, person_id))""")


def down(cursor):
    ...

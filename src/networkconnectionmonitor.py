from gi.repository import GObject
from gi.repository import Gio


class NetworkConnectionMonitor(GObject.Object):
    __gtype_name__ = "ConfyNetworkConnectionMonitor"

    def __init__(self):
        super().__init__()
        self._isconnected = False
        self.nm = Gio.NetworkMonitor.get_default()
        self.nm.connect("network-changed", self.on_network_changed)
        self.on_network_changed()

    def on_network_changed(self, *args):
        _ic = self._isconnected
        self._isconnected = self.nm.get_connectivity() == Gio.NetworkConnectivity.FULL
        if (self._isconnected != _ic):
            # print("NetworkConnectionMonitor.isconnected", self._isconnected)
            self.notify("isconnected")

    @GObject.Property(type=bool, default=False)
    def isconnected(self):
        """The system can reach the global netowrk. Bool, read-only"""
        # print("NetworkConnectionMonitor.isconnected getter:", self._isconnected)
        return self._isconnected

    def get_isconnected(self) -> bool:
        return self._isconnected

<img src="https://kirgroup.net/confy/logo.svg">

# Confy

Conferences schedule viewer for GNOME

Navigate Conference schedules, mark favourite talks

Built with Python, GTK4 and Libadwaita

- [Home Page](https://confy.kirgroup.net)
- [Project Page](https://sr.ht/~fabrixxm/Confy)
- [Patches and discussions](https://lists.sr.ht/~fabrixxm/confy-dev)
- [Tickets](https://todo.sr.ht/~fabrixxm/confy)


## Packages

[![Packaging status](https://repology.org/badge/vertical-allrepos/confy.svg)](https://repology.org/project/confy/versions)

## Build

Requirements

- GTK4
- Libadwaita
- Python3
- python-gobjects
- python-icalendar
- meson and ninja
- blueprint-compiler (tested with version 0.8.1 and 0.10.0)

### Build with Gnome Builder

Use "Clone repository..." from Open window, press "Run"

### Build from command line

    git clone https://git.sr.ht/~fabrixxm/confy
    cd confy
    meson setup . _build
    ninja -C _build
    ninja -C _build install

## Issues and Patches

Send issues to [~fabrixxm/confy@todo.sr.ht](https://todo.sr.ht/~fabrixxm/confy)

Send patches to [~fabrixxm/confy-dev@lists.sr.ht](https://lists.sr.ht/~fabrixxm/confy-dev)


